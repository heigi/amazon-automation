# Script to perform an automatic purchase on Amazon #


---

## Description

This is an improvement of the existing [amazon-autmation script](https://github.com/yosh1/amazon-automation). This script handles `503 Service Unavailable` HTTP error and keeps retrying until action can be performed.

---

## Installation

This script uses [selenium](https://www.selenium.dev/documentation/fr/selenium_installation/installing_selenium_libraries/#_python_) library and requires to install [chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads). Please refer to the links for more documentation.

---

## Configuration

Open [`main.py`](./main.py) with an editor and edit configuration. 

1. Replace `USERNAME` and `PASSWORD` with your Amazon login/password.
2. Edit `CHROMEDRV_PATH` with your Chromedriver path. Leave it empty if in system PATH.
3. Replace `ITEM_URL` value with the URL of your item.
4. Edit the remaining items according to your language.


## Execution

Run 

    $ python3 main.py

