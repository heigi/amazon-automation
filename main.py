#coding: utf-8

import time
from datetime import datetime
from selenium import webdriver

### CONFIGURATION ###

LOGIN_ID = '[username]'
LOGIN_PASSWORD = '[password]'

CHROMEDRV_PATH = '/path/to/chromedriver/'  # Chrome driver path (can be empty if in the PATH)

ITEM_URL = 'https://www.amazon.fr/PlayStation-officielle-DualSense-rechargeable-Bluetooth/dp/B08H93ZRK9/'    # URL of your item
CART_URL = 'https://www.amazon.fr/gp/cart/view.html/ref=nav_cart'

LIMIT_VALUE = 550   # MAX PRICE in the corresponding currency
IN_STOCK = 'En stock.'  # translate according to language
BY_KEYWORD = 'par' # Must match 'Ships from and sold ' BY_KEYWORD ' Amazon.com' (Note: use corresponding translation for other countries)
ACCEPT_SHOP = 'Amazon'  # Amazon shop only
CURR_LOC = 0 # 1 if currency is placed before amount (e.g. $9.99), 0 otherwise (9.99€)
THSTH_DELIM = '.'  # thousandth delimiter (e.g. ',' for US)
DECIMAL_DELIM = ','  # decimal delimiter (e.g. '.' for US)

REFRESH_DELAY_SEC = 5  # Delay before refresh in case of item unavailability or HTTP error
ITEM_DETECTION_TIMEOUT_SEC = 5 # Maximum time allowed to find an element in the page (element may take time to appear if page is loaded asynchronously)

### 

def l(str):
    print("%s : %s"%(datetime.now().strftime("%Y/%m/%d %H:%M:%S"),str))

if __name__ == '__main__':

    # Start browser and load Item page
    try:
        b = webdriver.Chrome(CHROMEDRV_PATH+'chromedriver')
        b.implicitly_wait(ITEM_DETECTION_TIMEOUT_SEC)
        b.get(ITEM_URL)
    except:
        l('Failed to open browser.')
        exit()

    while True:
        # Item page - Check inventory
        while True:
            try:
                # Confirm availability
                availability = b.find_element_by_id('availability').text
                if IN_STOCK not in availability:
                    raise Exception("not available yet.")

                # Confirm supplier
                shop = b.find_element_by_id('merchant-info').text
                shop = shop.split(BY_KEYWORD)[1].split('.')[0].strip()

                if ACCEPT_SHOP not in shop:
                    raise Exception("not Amazon.")

                # Add to cart
                b.find_element_by_id('add-to-cart-button').click()
                break
            except Exception as err:
                l(err)
                time.sleep(REFRESH_DELAY_SEC)
                b.refresh()

        # Cart list page - Proceed to checkout
        while True:
            try:
                b.get(CART_URL)
                c = b.find_element_by_name('proceedToRetailCheckout').click()
                break
            except Exception as err:
                l(err)
                time.sleep(REFRESH_DELAY_SEC)
                b.refresh() # might generate a 'send form' alert
                b.switch_to.alert.accept()

        # Login (optional) - TODO: Force login at the beginning
        try:
            b.find_element_by_id('ap_email').send_keys(LOGIN_ID)
            b.find_element_by_id('continue').click()
            b.find_element_by_id('ap_password').send_keys(LOGIN_PASSWORD)
            b.find_element_by_id('continue').click()
            b.find_element_by_id('signInSubmit').click()
        except:
            l('LOGIN SKIPPED.')
            pass

        # Summary page - Confirm price
        while True:
            try:
                p = b.find_element_by_css_selector('td.grand-total-price').text
                if not p:
                    raise Exception("Summary page unavailable.")
                break
            except Exception as err:
                l(err)
                time.sleep(REFRESH_DELAY_SEC)
                b.refresh() # might generate a 'send form' alert
                b.switch_to.alert.accept()

        if float(p.split(' ')[CURR_LOC].replace(THSTH_DELIM, '').replace(DECIMAL_DELIM,'.')) > LIMIT_VALUE:
            l('PRICE IS TOO LARGE.')
            continue

        # Confirm order
        b.find_element_by_name('placeYourOrder1').click()

        # Order confirmation page
        while True:
            try:
                p = b.find_element_by_id('widget-purchaseConfirmationStatus').text
                if not p:
                    raise Exception("Confirmation page unavailable.")
                break
            except Exception as err:
                l(err)
                time.sleep(REFRESH_DELAY_SEC)
                b.refresh()
        break

    l('ALL DONE.')